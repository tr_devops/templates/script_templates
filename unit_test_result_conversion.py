import csv, json

instructions_covered = 0.0
instructions_missed = 0.0
lines_covered = 0.0
lines_missed = 0.0

with open('./jacocoCsv') as infile:
    csvreader = csv.DictReader(infile, delimiter=',')
    for row in csvreader:
        # print(row['INSTRUCTION_COVERED'])
        # coverage = (float(row['INSTRUCTION_COVERED'])/(float(row['INSTRUCTION_COVERED']) + float(row['INSTRUCTION_MISSED']))) * 100
        # print('Class: {1} Coverage: {0}'.format(coverage, row['CLASS']))
        instructions_covered += float(row['INSTRUCTION_COVERED'])
        instructions_missed += float(row['INSTRUCTION_MISSED'])
        lines_covered += float(row['LINE_COVERED'])
        lines_missed += float(row['LINE_MISSED'])

total_coverage = (instructions_covered/(instructions_covered + instructions_missed)) * 100
line_coverage = (lines_covered/(lines_covered + lines_missed)) * 100
print("TOTAL CLASS COVERAGE (across all classes): {0} %".format(total_coverage))
print("TOTAL LINE COVERAGE (across all classes): {0} %".format(line_coverage))
json_out = {}
json_out["classes"] = total_coverage
json_out["lines"] = line_coverage

with open("./jacoco.json","w") as outfile:
    outfile.write(json.dumps(json_out, indent = 4))
